#!/bin/sh
cd /app/aquarium-sensors/

pkill -f "/usr/local/bin/python3.7 /app/aquarium-sensors/Main.py"

sleep 10

modprobe w1-gpio
modprobe w1-therm
/usr/local/bin/python3.7 /app/aquarium-sensors/Main.py &
