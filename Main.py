from time import sleep
import configparser
from AppLogger import logger as app_logger
import logging


def main():
    app_logger.info("========== Application starting ==========")

    config = parse_config()
    console_log_level = logging.getLevelName(config['DEFAULT']['console_log_level'])
    file_log_level = logging.getLevelName(config['DEFAULT']['file_log_level'])
    app_logger.setLevel(console_log_level)
    app_logger.setLevel(file_log_level)
    temp_moisture_runner(config)
    dosing_actuator_runner(config)
    co2_runner(config)


def temp_moisture_runner(config):
    try:
        from TemperatureMoisture import TemperatureMoisture
        temp_moisture = TemperatureMoisture(config)
        temp_moisture.start()
    except Exception as e:
        app_logger.error("Temperature/moisture module not working")
        app_logger.error(f"Trace: {e}")
        sleep(60)
        temp_moisture_runner(config)


def co2_runner(config):
    try:
        from Co2Monitor import Co2Monitor
        co2_monitor = Co2Monitor(config)
        co2_monitor.start()
    except Exception as e:
        app_logger.error("CO2 module not working")
        app_logger.error(f"Trace: {e}")
        sleep(60)
        co2_runner(config)


def dosing_actuator_runner(config):
    try:
        from DosingActuator import DosingActuator
        dosing_ac = DosingActuator(config)
        dosing_ac.start()
    except Exception as e:
        app_logger.error("Dosing module not working")
        app_logger.error(f"Trace: {e}")
        sleep(60)
        dosing_actuator_runner(config)


def parse_config():
    try:
        config = configparser.ConfigParser()
        config.read('config/Default.conf')
        return config
    except Exception as e:
        app_logger.error("Failed to load config")
        app_logger.error(f"Trace: {e}")


main()
while True:
    pass
