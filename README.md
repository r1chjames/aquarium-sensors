## Setting up

*Requires Python 3.6+*

* Set execute permissions on install.sh: `chmod +x install.sh`
* Run install.sh to set the app up `sudo ./install.sh`
