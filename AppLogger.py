import logging

logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Create handlers
c_handler = logging.StreamHandler()
f_handler = logging.FileHandler('app.log')

# Create formatters and add it to handlers
c_format = logging.Formatter('%(asctime)s-%(levelname)s-%(funcName)s-%(message)s')
f_format = logging.Formatter('%(asctime)s-%(levelname)s-%(funcName)s-%(message)s')
c_handler.setFormatter(c_format)
f_handler.setFormatter(f_format)

# Add handlers to the logger
logger.addHandler(c_handler)
logger.addHandler(f_handler)