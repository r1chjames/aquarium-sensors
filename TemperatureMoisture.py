import os
import glob
import sys
import uuid

from AppLogger import logger as app_logger
import traceback
from time import sleep
from threading import Thread
import RPi.GPIO as GPIO
from MqttClient import MqttClient

os.system('modprobe w1-gpio')
os.system('modprobe w1-therm')

base_dir = '/sys/bus/w1/devices/'
device_folder = glob.glob(base_dir + '28*')[0]
device_file = device_folder + '/w1_slave'
previous_temp = "0"
previous_moisture = ""


class TemperatureMoisture(Thread):

    def __init__(self, config):
        self._config = config
        # self.logger = logging.getLogger(__name__)
        self.temperature_topic = config['DEFAULT']['temperature_topic']
        self.moisture_topic = config['DEFAULT']['moisture_topic']
        self.actuation_topic = config['DEFAULT']['actuation_topic']
        self.moisture_bcm_gpio = int(config['DEFAULT']['moisture_bcm_gpio'])
        client = config['DEFAULT']['mqtt_client'] + "_temp_humidity"

        self._mqtt_client = MqttClient(config, client)
        self._mqtt_client.client.loop_start()
        Thread.__init__(self)
        self.daemon = True

    @staticmethod
    def read_temp_raw():
        f = open(device_file, 'r')
        lines = f.readlines()
        f.close()
        # lines = ['9c 01 4b 46 7f ff 0c 10 0c : crc=0c YES', '9c 01 4b 46 7f ff 0c 10 0c t=25750']
        return lines

    def convert_to_high_low(self, value):
        if value == 0:
            return 100
        else:
            return 0

    def read_moisture(self):
        try:
            GPIO.setmode(GPIO.BCM)
            GPIO.setup(self.moisture_bcm_gpio, GPIO.IN)
            return self.convert_to_high_low(GPIO.input(self.moisture_bcm_gpio))
            # return 5
        except Exception as e:
            app_logger.error(f"Temperature/moisture sensor error: {e}")
            traceback.print_tb(sys.exc_info()[2])
            raise

    def read_temp(self):
        lines = self.read_temp_raw()
        while lines[0].strip()[-3:] != 'YES':
            sleep(0.2)
            lines = self.read_temp_raw()
        equals_pos = lines[1].find('t=')
        temp_c = 0
        if equals_pos != -1:
            temp_string = lines[1][equals_pos + 2:]
            temp_c = float(temp_string) / 1000.0
        return temp_c

    def process_temperature(self, transaction_id):
        new_temp = str(round(self.read_temp(), 2))
        global previous_temp
        if new_temp != previous_temp:
            rc = self._mqtt_client.send_message(transaction_id, self.temperature_topic, new_temp)
            previous_temp = new_temp
            app_logger.debug(f"ID: {str(transaction_id)} - Temperature confirmation status: {str(rc)}")
        else:
            app_logger.debug(f"ID: {str(transaction_id)} - Temperature unchanged")

    def process_moisture(self, transaction_id):
        new_moisture = str(self.read_moisture())
        global previous_moisture
        if new_moisture != previous_moisture:
            rc = self._mqtt_client.send_message(transaction_id, self.moisture_topic, new_moisture)
            previous_moisture = new_moisture
            app_logger.debug(f"ID: {str(transaction_id)} - Temperature confirmation status: {str(rc)}")
        else:
            app_logger.debug(f"ID: {str(transaction_id)} - Moisture unchanged")

    def run(self):
        try:
            while True:
                transaction_id = uuid.uuid4()
                app_logger.debug(f"ID: {str(transaction_id)} - Beginning temperature/moisture sensor loop")
                self.process_moisture(transaction_id)
                self.process_temperature(transaction_id)
                sleep(30)
                app_logger.debug(f"ID: {str(transaction_id)} - Ended temperature/moisture sensor loop")
        except KeyboardInterrupt:
            self._mqtt_client.client.loop_stop()
            GPIO.cleanup()
        except Exception as e:
            app_logger.error(f"Temperature/moisture sensor error: {e}")
            traceback.print_tb(sys.exc_info()[2])
            raise
