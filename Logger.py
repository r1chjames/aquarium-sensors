import logging

logging.basicConfig(filename='app.log', filemode='w', format='%(asctime)s-%(levelname)s-%(funcName)s-%(message)s')

def info(message):
    logging.info(message)

def debug(message):
    logging.debug(message)

def warning(message):
    logging.warning(message)

def error(message):
    logging.error(message)
