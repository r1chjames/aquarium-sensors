#!/usr/bin/env bash

echo Updating system
apt-get update

echo Installing dependencies
apt-get install build-essential tk-dev libncurses5-dev libncursesw5-dev libreadline6-dev libdb5.3-dev libgdbm-dev libsqlite3-dev libssl-dev libbz2-dev libexpat1-dev liblzma-dev zlib1g-dev libffi-dev

cd /app
echo Downloading Python 3.7
wget https://www.python.org/ftp/python/3.7.1/Python-3.7.1.tar.xz

echo Extracting Python 3.7
tar xf Python-3.7.1.tar.xz
cd Python-3.7.1

echo Configuring Python
./configure

echo Making Python
make
echo Making Python Install
make altinstall

cd /app/aquarium-sensors/
echo Installing pip3 dependencies
pip3.7 install --upgrade distribute
pip3.7 install ipython
pip3.7 install -r requirements.txt

echo Adding Cron entry
(crontab -l -u pi 2>/dev/null || true; echo "@reboot /app/aquarium-sensors/run.sh") | crontab -u pi -