#!/bin/sh
sudo mkdir /app
sudo chmod 777 -R /app

pkill -f "AquariumMqtt.py"

mkdir /app/sensors
cp run.sh /app/sensors
cp AquariumMqtt.py /app/sensors

grep '@reboot /app/sensors/run.sh' /etc/crontab || echo '@reboot /app/sensors/run.sh' >> /etc/crontab

/app/sensors/run.sh